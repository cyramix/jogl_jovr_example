/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cyramix.cyramixvr_example;

import com.jogamp.newt.NewtFactory;
import com.jogamp.newt.Window;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.util.FPSAnimator;
import com.oculusvr.capi.EyeRenderDesc;
import com.oculusvr.capi.FovPort;
import com.oculusvr.capi.GLTexture;
import com.oculusvr.capi.Hmd;
import com.oculusvr.capi.HmdDesc;
import com.oculusvr.capi.LayerEyeFov;
import com.oculusvr.capi.OvrLibrary;
import static com.oculusvr.capi.OvrLibrary.ovrProjectionModifier.ovrProjection_ClipRangeOpenGL;
import static com.oculusvr.capi.OvrLibrary.ovrProjectionModifier.ovrProjection_RightHanded;
import com.oculusvr.capi.OvrMatrix4f;
import com.oculusvr.capi.OvrRecti;
import com.oculusvr.capi.OvrSizei;
import com.oculusvr.capi.OvrVector3f;
import com.oculusvr.capi.Posef;
import com.oculusvr.capi.SwapTextureSet;
import com.oculusvr.capi.ViewScaleDesc;
import java.util.Random;
import javax.media.opengl.GL;
import static javax.media.opengl.GL.GL_COLOR_BUFFER_BIT;
import static javax.media.opengl.GL.GL_DEPTH_BUFFER_BIT;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;

/**
 *
 * @author cramirez
 */
public class JoglJovrExample implements GLEventListener {

    private static final int EYE_COUNT = 2;
    private static Hmd hmd;

    private static boolean usingHmd;

    private static final OvrVector3f[] eyeOffsets = OvrVector3f.buildPair();
    private static final FovPort[] fovPorts
            = FovPort.buildPair();
    private static final Posef[] poses
            = Posef.buildPair();

    private static int frameCount = -1;

    static Random random = new Random();
    private static GLWindow glWindow;
    private static HmdDesc hmdDesc;
    private static final OvrSizei[] textureSizes = new OvrSizei[EYE_COUNT];
    private static int width;
    private static int height;
    private static SwapTextureSet swapTexture;
    private static GLTexture mirrorTexture;
    private static final LayerEyeFov layer = new LayerEyeFov();
    private static final ViewScaleDesc viewScaleDesc = new ViewScaleDesc();
    private static OvrSizei doubleSize;

    private static int frameBufferId;

    private static void initializeHmd() {
        Hmd.initialize();

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        hmd = Hmd.create();
        if (null == hmd) {
            usingHmd = false;
            throw new IllegalStateException(
                    "Unable to initialize HMD");
        }

        usingHmd = true;
        hmdDesc = hmd.getDesc();

        width = 1024;
        height = 768;

        hmd.configureTracking();

        for (int eye = 0; eye < EYE_COUNT; ++eye) {
            fovPorts[eye] = hmdDesc.DefaultEyeFov[eye];
            OvrMatrix4f m = Hmd.getPerspectiveProjection(fovPorts[eye], 0.1f, 1000000f, ovrProjection_RightHanded
                    | ovrProjection_ClipRangeOpenGL);

            textureSizes[eye] = hmd.getFovTextureSize(eye, fovPorts[eye], 1.0f);
        }

    }

    private static void configureHmd(GL gl) {

        doubleSize = new OvrSizei();
        doubleSize.w = textureSizes[0].w + textureSizes[1].w;
        doubleSize.h = textureSizes[0].h;

        swapTexture = hmd.createSwapTexture(doubleSize, GL.GL_RGBA);
        mirrorTexture = hmd.createMirrorTexture(new OvrSizei(width, height), GL.GL_RGBA);

        layer.Header.Type = OvrLibrary.ovrLayerType.ovrLayerType_EyeFov;
        layer.ColorTexure[0] = swapTexture;
        layer.Fov = fovPorts;
        layer.RenderPose = poses;
        for (int eye = 0; eye < EYE_COUNT; ++eye) {
            layer.Viewport[eye].Size = textureSizes[eye];
        }
        layer.Viewport[1].Pos.x = layer.Viewport[1].Size.w;
        layer.Viewport[1].Pos.y = 0;
        
        int[] tempId = new int[1];
        gl.glGenFramebuffers(GL2.GL_FRAMEBUFFER, tempId, 0);
        frameBufferId = tempId[0];

        for (int eye = 0; eye < 2; ++eye) {
            EyeRenderDesc eyeRenderDesc = hmd.getRenderDesc(eye, fovPorts[eye]);
            eyeOffsets[eye].x = eyeRenderDesc.HmdToEyeViewOffset.x;
            eyeOffsets[eye].y = eyeRenderDesc.HmdToEyeViewOffset.y;
            eyeOffsets[eye].z = eyeRenderDesc.HmdToEyeViewOffset.z;
        }
        viewScaleDesc.HmdSpaceToWorldScaleInMeters = 1.0f;
    }

    private static void endFrame() {
        hmd.submitFrame(frameCount, layer);
        swapTexture.CurrentIndex++;
        swapTexture.CurrentIndex %= swapTexture.TextureCount;
    }

    private static void createWindow() {
        GLProfile glProfile = GLProfile.get(GLProfile.GL2);
        System.out.println("got: " + glProfile.getImplName());
        final Window window = NewtFactory.createWindow(new GLCapabilities(glProfile));

        window.setSize(width, height);
        window.setPosition(0, 0);

        glWindow = GLWindow.create(window);
        glWindow.setAutoSwapBufferMode(false);
        glWindow.setUndecorated(true);
        glWindow.addGLEventListener(new JoglJovrExample());
        glWindow.setVisible(true);

    }

    public void init(GLAutoDrawable glad) {

        JoglJovrExample.configureHmd(glad.getGL());

    }

    public void dispose(GLAutoDrawable glad) {
        final GL gl = glad.getGL();
        gl.glDeleteFramebuffers(GL2.GL_FRAMEBUFFER, new int[]{frameBufferId}, 0);
        hmd.destroy();
    }

    public void display(GLAutoDrawable glad) {
        renderFrame(glad);

    }

    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {
    }

    private static void renderFrame(GLAutoDrawable glad) {
        GL2 gl = glad.getGL().getGL2();

        ++frameCount;
        Posef eyePoses[] = hmd.getEyePoses(frameCount, eyeOffsets);

        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, frameBufferId);
        final int swapTextId = swapTexture.getTexture(swapTexture.CurrentIndex).ogl.TexId;
        gl.glFramebufferTexture2D(GL.GL_FRAMEBUFFER, GL.GL_COLOR_ATTACHMENT0,
                GL.GL_TEXTURE_2D, swapTextId, 0);

        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClearDepth(1.0f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        for (int eye = 0; eye < EYE_COUNT; ++eye) {
            
            OvrRecti vp = layer.Viewport[eye];
            gl.glScissor(vp.Pos.x, vp.Pos.y, vp.Size.w, vp.Size.h);
            gl.glViewport(vp.Pos.x, vp.Pos.y, vp.Size.w, vp.Size.h);

            Posef pose = eyePoses[eye];
            // This doesn't work as it breaks the contiguous nature of the array
            // FIXME there has to be a better way to do this
            poses[eye].Orientation = pose.Orientation;
            poses[eye].Position = pose.Position;

            renderScene(gl);
        }

        gl.glBindFramebuffer(GL.GL_FRAMEBUFFER, 0);

        endFrame();

        drawMirrorTexture(gl);
        glad.swapBuffers();
    }

    private static void drawMirrorTexture(GL2 gl) {
        gl.glDisable(GL2.GL_LIGHTING);
        gl.glColor3f(1, 1, 1.0f);
        gl.glEnable(GL.GL_TEXTURE_2D);
        gl.glBindTexture(GL.GL_TEXTURE_2D, mirrorTexture.ogl.TexId);

        gl.glScissor(0, 0, width, height);
        gl.glViewport(0, 0, width, height);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        gl.glBegin(GL2.GL_QUADS);
        gl.glTexCoord2d(0, 1);
        gl.glVertex2d(-1, -1);
        gl.glTexCoord2d(1, 1);
        gl.glVertex2i(1, -1);

        gl.glTexCoord2d(1, 0);
        gl.glVertex2i(1,
                1);

        gl.glTexCoord2d(0, 0);
        gl.glVertex2d(-1, 1);
        gl.glEnd();
        gl.glBindTexture(GL.GL_TEXTURE_2D, 0);
    }

    private static void renderScene(GL2 gl) {

        gl.glDisable(GL2.GL_LIGHTING);

        gl.glColor3f(0.5f, 0.5f, System.currentTimeMillis() % 1000 / 1000.0f);

        gl.glBegin(GL.GL_TRIANGLE_STRIP);

        gl.glVertex2d(-1, -1);
        gl.glVertex2d(-1, 1);

        gl.glVertex2d(1, 1);

        gl.glEnd();
    }

    public static void main(String[] args) {
        initializeHmd();
        createWindow();

        FPSAnimator animator = new FPSAnimator(75);
        animator.add(glWindow);
        animator.start();
    }
}
